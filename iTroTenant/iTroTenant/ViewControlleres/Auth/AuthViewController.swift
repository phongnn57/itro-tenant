import UIKit
import CarbonKit
class AuthViewController: UIViewController, CarbonTabSwipeNavigationDelegate {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    var carbonTabSwipeNavication = CarbonTabSwipeNavigation()
    override func viewDidLoad() {
        super.viewDidLoad()
        let items = ["LoginViewController", "RegisterViewController"]
        carbonTabSwipeNavication = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavication.insert(intoRootViewController: self)
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
//        guard let storyboard = storyboard else { return UIViewController() }
        if index == 0 {
            return storyboard!.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        }
        else {
            return storyboard!.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        }
    }
    
    @IBAction func indexChange(_ sender: Any) {
    
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            carbonTabSwipeNavication.setCurrentTabIndex(0, withAnimation: true)
        case 1:
              carbonTabSwipeNavication.setCurrentTabIndex(1, withAnimation: true)
        default:
            break
        }
    }
    
    
}
